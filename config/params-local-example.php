<?php

return [
    'wg' => [
        'nic' => 'assguard',
        'subnet' => '10.2.0.0/16',
        'endpoint' => 'ip_address',
        'port' => 5223,
        'ip_address' => 'ip_address',
        'framed_ip_address' => '10.2.0.1',
        'public_key' => 'server_public_key',
        'dns' => [
            '10.0.0.10',
            '10.1.0.1',
        ],
    ],
    'radius' => [
        'enabled' => false,
        'server' => 'radius.domain.com',
        'eap_secret' => 'eap_password',
        'acct-interval' => 600, // 10 minutes
        'handshake-timeout' => 86400, // 24 hours
    ],
    'postauth' => [
        'connect' => 'https://domain.com/server/connect/',
        'disconnect' => 'https://domain.com/server/disconnect/',
        'timeout' => 50,
    ],
    'lajupostauth' => [
        'connect' => 'https://lajudomain.com/server/connect/',
        'disconnect' => 'https://lajudomain.com/server/disconnect/',
        'timeout' => 10,
    ],
];
