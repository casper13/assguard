<?php

use yii\web\Response;

$params = require __DIR__ . '/params.php';
if (file_exists(__DIR__ . '/params-local.php')) {
    $paramsLocal = require __DIR__ . '/params-local.php';
}
if (!empty($paramsLocal)) {
    $params = \yii\helpers\ArrayHelper::merge($params, $paramsLocal);
}
$urlManagerConfig = require(__DIR__ . '/urlManager.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'formatter' => [
            'defaultTimeZone' => 'UTC',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'locale' => 'en-US',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'jJocaBss8UfYqseo9tP6CiZTa6DzYNsX',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'multipart/form-data' => 'yii\web\MultipartFormDataParser',
            ],
        ],
        'response' => [
            //'format' => Response::FORMAT_JSON,
            'on beforeSend' => function ($event) {
                /** @var Response $response */
                $response = $event->sender;
                if ($response->data !== null && $response->format == Response::FORMAT_JSON) {
                    if ($response->isSuccessful) {
                        $response->data = [
                            'success' => $response->isSuccessful,
                            'data' => $response->data,
                            'errors' => [],
                        ];
                    } else {
                        if ($response->statusCode == 422) {
                            $response->data = [
                                'success' => $response->isSuccessful,
                                'data' => new \stdClass,
                                'errors' => $response->data,
                            ];
                        } else {
                            $response->data = [
                                'success' => $response->isSuccessful,
                                'data' => new \stdClass,
                                'errors' => [
                                    $response->data,
                                ],
                            ];
                        }
                    }
                }
            },
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
        ],
        'mutex' => [
            'class' => 'yii\redis\Mutex',
        ],
        'cache' => [
            'class' => '\yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableSession' => false,
            'enableAutoLogin' => false,
            'loginUrl' => null,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => $urlManagerConfig,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:418',
                        'yii\web\HttpException:422',
                        'yii\web\HttpException:501',
                        'radius',
                    ],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:418',
                        'yii\web\HttpException:422',
                        'yii\web\HttpException:501',
                        'radius',
                    ],
                    'logVars' => [],
                    'logFile' => '@app/runtime/logs/pretty/app.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => [
                        'error',
                        'warning',
                        //'info',
                        //'trace',
                    ],
                    'categories' => ['yii\web\HttpException:400', ],
                    'logFile' => '@app/runtime/logs/400/app.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => [
                        'error',
                        'warning',
                        //'info',
                        //'trace',
                    ],
                    'categories' => ['yii\web\HttpException:401', ],
                    'logFile' => '@app/runtime/logs/401/app.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => [
                        'error',
                        'warning',
                        //'info',
                        //'trace',
                    ],
                    'categories' => ['yii\web\HttpException:403', ],
                    'logFile' => '@app/runtime/logs/403/app.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => [
                        'error',
                        'warning',
                        //'info',
                        //'trace',
                    ],
                    'categories' => ['yii\web\HttpException:404', ],
                    'logFile' => '@app/runtime/logs/404/app.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => [
                        'error',
                        'warning',
                        //'info',
                        //'trace',
                    ],
                    'categories' => ['yii\web\HttpException:418', ],
                    'logFile' => '@app/runtime/logs/418/app.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => [
                        'error',
                        'warning',
                        //'info',
                        //'trace',
                    ],
                    'categories' => ['yii\web\HttpException:422', ],
                    'logFile' => '@app/runtime/logs/422/app.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => [
                        'error',
                        'warning',
                        //'info',
                        //'trace',
                    ],
                    'categories' => ['yii\web\HttpException:501', ],
                    'logFile' => '@app/runtime/logs/501/app.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'levels' => [
                        'error',
                        'warning',
                        //'info',
                    ],
                    'categories' => ['radius',],
                    'logVars' => [],
                    'logFile' => '@app/runtime/logs/radius/app.log',
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => [
            '127.0.0.1',
            '::1',
            '192.168.83.*',
            '192.168.56.*',
        ],
        'disableIpRestrictionWarning' => true,
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => [
            '127.0.0.1',
            '::1',
            '192.168.83.*',
            '192.168.56.*',
        ],
    ];
}

return $config;
