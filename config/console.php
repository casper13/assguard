<?php

$params = require __DIR__ . '/params.php';
if (file_exists(__DIR__ . '/params-local.php')) {
    $paramsLocal = require __DIR__ . '/params-local.php';
}
if (!empty($paramsLocal)) {
    $params = \yii\helpers\ArrayHelper::merge($params, $paramsLocal);
}

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'formatter' => [
            'defaultTimeZone' => 'UTC',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'locale' => 'en-US',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
        ],
        'mutex' => [
            'class' => 'yii\redis\Mutex',
        ],
        'cache' => [
            'class' => '\yii\caching\FileCache',
        ],
        'log' => [
            'flushInterval' => 1,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'exportInterval' => 1,
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'radius',
                    ],
                    'logFile' => '@app/runtime/logs/command.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'exportInterval' => 1,
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'radius',
                    ],
                    'logVars' => [],
                    'logFile' => '@app/runtime/logs/pretty/command.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'enabled' => YII_DEBUG,
                    'exportInterval' => 1,
                    'levels' => [
                        'error',
                        'warning',
                        //'info',
                    ],
                    'categories' => ['radius',],
                    'logVars' => [],
                    'logFile' => '@app/runtime/logs/radius/command.log',
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['controllerMap']['stubs'] = [
        'class' => 'bazilio\stubsgenerator\StubsController',
    ];
}

return $config;
