<?php

return [
    'class' => '\yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'suffix' => '/',
    'normalizer' => [
        'class' => 'yii\web\UrlNormalizer',
        'collapseSlashes' => true,
        'normalizeTrailingSlash' => true,
    ],
    'rules' => [
        // static site rules
        '/' => 'site/index',
        '<controller:\w+(\-[\w]+)*>/' => '<controller>/index',
        //'<controller:\w+(\-[\w]+)*>/<id:[\d]+>' => '<controller>/view',
        //'<controller:\w+(\-[\w]+)*>/<action:\w+(\-[\w]+)*>/<id:[\d]+>' => '<controller>/<action>',
        //'<controller:\w+(\-[\w]+)*>/<action:\w+(\-[\w]+)*>' => '<controller>/<action>',
    ],
];
