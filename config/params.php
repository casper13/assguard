<?php

return [
    'wg' => [
        'nic' => 'assguard',
        'subnet' => '10.2.0.0/16',
        //'endpoint' => 'server_ip_address',
        'port' => 5223,
        //'ip_address' => 'server_ip_address',
        'framed_ip_address' => '10.2.0.1',
        //'public_key' => 'server_public_key',
        'dns' => [
            '10.0.0.10',
            '10.1.0.1',
        ],
    ],
    'radius' => [
        //'server' => 'radius_server_ip_address',
        //'eap_secret' => 'radius_server_eap_password',
        'acct-interval' => 600, // 10 minutes
        'handshake-timeout' => 86400, // 24 hours
    ],
    'postauth' => [
        //'connect' => 'https://domain.com/server/connect/',
        //'disconnect' => 'https://domain.com/server/disconnect/',
        'timeout' => 10,
    ],
    'lajupostauth' => [
        //'connect' => 'https://laju.domain.com/server/connect/',
        //'disconnect' => 'https://laju.domain.com/server/disconnect/',
        'timeout' => 10,
    ],
];
