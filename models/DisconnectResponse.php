<?php

namespace app\models;

class DisconnectResponse extends \yii\base\Model
{
    /**
     * @var string
     */
    public $username = '';
    /**
     * @var int
     */
    public $rx = 0;
    /**
     * @var int
     */
    public $tx = 0;
}
