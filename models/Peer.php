<?php

namespace app\models;

use app\services\AddressService;
use Yii;
use yii\base\Exception;
use yii\redis\ActiveRecord;

/**
 * Peer model
 *
 * @property string $session_id
 * @property string $username
 * @property string $password
 * @property string $public_key
 * @property string $ip_address
 * @property string $framed_ip_address
 * @property int $input_octets
 * @property int $output_octets
 * @property int $connected_at
 * @property int $accounted_at
 * @property int $latest_handshakes_at
 *
 * @property-read string $cidrIpAddress
 */
class Peer extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function primaryKey(): array
    {
        return ['public_key'];
    }

    /**
     * @inheritDoc
     */
    public function attributes(): array
    {
        return [
            'username',
            'session_id',
            'password',
            'public_key',
            'ip_address',
            'framed_ip_address',
            'input_octets',
            'output_octets',
            'connected_at',
            'accounted_at',
            'latest_handshakes_at',
        ];
    }

    /**
     * @param string $prefix
     * @param int $length
     * @return string
     * @throws Exception
     */
    public static function generateString(string $prefix, int $length): string
    {
        $sessionId = (string)$prefix;
        if (!empty($sessionId)) {
            $sessionId .= '-';
        }
        $prefixLength = strlen($prefix);
        $suffixLength = $length - $prefixLength;
        if ($suffixLength > 0) {
            $sessionLength = $prefixLength;
            while ($sessionLength < $length) {
                $char = Yii::$app->security->generateRandomString(1);
                if ($char != '_' && $char != '-') {
                    $sessionId .= $char;
                }
                $sessionLength = strlen($sessionId);
            }
        }

        return strtoupper($sessionId);
    }

    /**
     * @param string $prefix
     * @param int $length
     * @throws Exception
     */
    public function generateSessionId(string $prefix, int $length = 16)
    {
        $this->session_id = static::generateString($prefix, $length);
    }

    /**
     * Acquire Framed IP Address
     */
    public function acquireFramedIp()
    {
        $service = new AddressService();
        $beginIp = $service->getFirstIp() + 2; // Start with next by server IP
        $endIp = $service->getLastIp();
        for ($longIp = $beginIp; $longIp <= $endIp; $longIp++) {
            $ipAddress = long2ip($longIp);
            $exists = Peer::find()->andWhere(['framed_ip_address' => $ipAddress])->exists();
            if (!$exists) {
                $this->framed_ip_address = $ipAddress;
                break;
            }
        }
        if (empty($this->framed_ip_address)) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getCidrIpAddress(): string
    {
        $addressService = new AddressService();;
        return $this->framed_ip_address . '/' . $addressService->getCidr();
    }

    /**
     * Find Peer from DB by WireGuard Public Key
     *
     * @param string $publicKey
     * @return static|null
     */
    public static function findByPublicKey($publicKey)
    {
        /** @var static $model */
        $model = static::find()->andWhere(['public_key' => $publicKey])->limit(1)->one();

        return $model;
    }

    /**
     * Find Peer from DB by Username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        /** @var static $model */
        $model = static::find()->andWhere(['username' => $username])->limit(1)->one();

        return $model;
    }
}
