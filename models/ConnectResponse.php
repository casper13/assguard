<?php

namespace app\models;

use yii\base\Model;

class ConnectResponse extends Model
{
    /**
     * A base64 Server public key calculated by wg pubkey from a private key, and usually transmitted out of band to the author
     * of the configuration file.
     *
     * @var string
     */
    public $publicKey = '';

    /**
     * An Server endpoint IP Address or hostname.
     *
     * @var string
     */
    public $endpoint = '';

    /**
     * A 16-bit Server port for listening.
     *
     * @var int
     */
    public $port = 0;

    /**
     * Client routing rule
     *
     * @var string
     */
    //public string $allowedIps = '0.0.0.0/0';

    /**
     * Array of client DNS servers ip addresses
     * @var string[] array of string
     */
    public $dns = [];

    /**
     * Client framed IP Address
     *
     * @var string
     */
    public $address = '';
}
