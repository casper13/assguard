<?php

namespace app\models;

use yii\base\Model;

class ApiRequest extends Model
{
    const SCENARIO_START = 'start';
    const SCENARIO_STOP = 'stop' ;
    /**
     * @var ?string
     */
    public $username = null;

    /**
     * @var ?string
     */
    public $password = null;

    /**
     * A base64 public key calculated by wg pubkey from a private key, and usually transmitted out of band to the author
     * of the configuration file. Required.
     *
     * @var ?string
     */
    public $publicKey = null;

    /**
     * @inheritDoc
     */
    public function formName(): string
    {
        return '';
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['username', 'password', 'publicKey', ], 'trim',],
            [['username', 'password', ], 'string', 'min' => 3, 'max' => 255,],
            [['publicKey', ], 'string', 'length' => 44,],
            [['username', 'publicKey',], 'required',],
            [['password',], 'required', 'on' => self::SCENARIO_START,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = Model::scenarios();
        $scenarios[self::SCENARIO_START] = ['username', 'password', 'publicKey',];
        $scenarios[self::SCENARIO_STOP] = ['username', 'publicKey',];

        return $scenarios;
    }
}
