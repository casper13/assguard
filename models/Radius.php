<?php

namespace app\models;

use app\helpers\MsChap;
use app\helpers\RadiusAcctStatusTypeHelper;
use app\helpers\RadiusAcctTerminateCauseType;
use app\helpers\RadiusAuthTypeHelper;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;

class Radius
{
    const LOG_CATEGORY = 'radius';

    public $server;
    public $secret;
    public $authType;
    public $authPort;
    public $acctPort = 1813;
    public $timeout = 5;
    public $retry = 3;
    public $acctInterval = 600;
    public $handshakeTimeout = 43200;

    /**
     * @var bool
     */
    private $enabled;


    public function __construct()
    {
        $this->enabled = (string)ArrayHelper::getValue(Yii::$app->params, 'radius.enabled', false);
        $this->server = (string)ArrayHelper::getValue(Yii::$app->params, 'radius.server', '127.0.0.1');
        $this->secret = (string)ArrayHelper::getValue(Yii::$app->params, 'radius.eap_secret');
        $this->authPort = (int)ArrayHelper::getValue(Yii::$app->params, 'radius.port', 1812);
        $this->acctPort = (string)ArrayHelper::getValue(Yii::$app->params, 'radius.acct_port', 1813);
        $this->timeout = (string)ArrayHelper::getValue(Yii::$app->params, 'radius.timeout', 5);
        $this->retry = (string)ArrayHelper::getValue(Yii::$app->params, 'radius.retry', 3);
        $this->authType = (string)ArrayHelper::getValue(Yii::$app->params, 'radius.auth_type', RadiusAuthTypeHelper::TYPE_PAP);
        $this->acctInterval = (int)ArrayHelper::getValue(Yii::$app->params, 'radius.acct-interval', 600);
        $this->handshakeTimeout = (int)ArrayHelper::getValue(Yii::$app->params, 'radius.handshake-timeout', 86400);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return (bool)$this->enabled;
    }

    /**
     * @return int
     */
    public function getAcctInterval(): int
    {
        return $this->acctInterval;
    }

    /**
     * @return int
     */
    public function getHandshakeTimeout(): int
    {
        return $this->handshakeTimeout;
    }

    /**
     * @param Nas $nas
     * @param Peer $peer
     * @param int $type
     * @param int $timestamp
     * @param int $terminateCauseType
     * @return bool
     */
    public function acct(
        Nas $nas,
        Peer $peer,
        int $timestamp,
        int $type,
        int $terminateCauseType = RadiusAcctTerminateCauseType::TYPE_RADIUS_TERM_USER_REQUEST
    ): bool
    {
        $handle = radius_acct_open();

        try {
            $status = radius_add_server($handle, $this->server, $this->acctPort, $this->secret, $this->timeout, $this->retry);
            if ($status) {
                $status = radius_create_request($handle, RADIUS_ACCOUNTING_REQUEST);
            }

            // acctsessionid
            if ($status) {
                $status = radius_put_string($handle, RADIUS_ACCT_SESSION_ID, $peer->session_id);
            }
            // username
            if ($status) {
                $status = radius_put_string($handle, RADIUS_USER_NAME, $peer->username);
            }
            // nasipaddress
            if ($status) {
                $status = radius_put_addr($handle, RADIUS_NAS_IP_ADDRESS, $nas->ip_address);
            }
            if ($status) {
                $status = radius_put_string($handle, RADIUS_NAS_IDENTIFIER, $nas->endpoint);
            }

            // nasportid
            if ($status) {
                $status = radius_put_int($handle, RADIUS_NAS_PORT, $nas->getConnectPort());
            }
            // nasporttype
            if ($status) {
                $status = radius_put_int($handle, RADIUS_NAS_PORT_TYPE, RADIUS_VIRTUAL);
            }
            // acctsessiontime
            if ($status) {
                $sessionTime = $timestamp - $peer->connected_at;
                $status = radius_put_int($handle, RADIUS_ACCT_SESSION_TIME, $sessionTime);
            }

            // acctauthentic
            if ($status) {
                $status = radius_put_int($handle, RADIUS_ACCT_AUTHENTIC, RADIUS_AUTH_RADIUS);
            }

            // acctinputoctets
            if ($status) {
                $status = radius_put_int($handle, RADIUS_ACCT_INPUT_OCTETS, $peer->input_octets);
            }

            // acctoutputoctets
            if ($status) {
                $status = radius_put_int($handle, RADIUS_ACCT_OUTPUT_OCTETS, $peer->output_octets);
            }
            // calledstationid
            if ($status) {
                $status = radius_put_string($handle, RADIUS_CALLED_STATION_ID, $nas->ip_address);
            }
            // callingstationid
            if ($status) {
                $status = radius_put_string($handle, RADIUS_CALLING_STATION_ID, $peer->ip_address);
            }

            if ($status) {
                $status = radius_put_int($handle, RADIUS_ACCT_STATUS_TYPE, $type);
            }
            switch ($type) {
                case RadiusAcctStatusTypeHelper::TYPE_RADIUS_STOP:
                    // acctterminatecause
                    if ($status) {
                        $status = radius_put_int($handle, RADIUS_ACCT_TERMINATE_CAUSE, $terminateCauseType);
                    }
                    break;
                case RadiusAcctStatusTypeHelper::TYPE_RADIUS_ALIVE:
                case RadiusAcctStatusTypeHelper::TYPE_RADIUS_ACCOUNTING_ON:
                case RadiusAcctStatusTypeHelper::TYPE_RADIUS_ACCOUNTING_OFF:
                default:
                    break;
            }


            // servicetype
            if ($status) {
                $status = radius_put_int($handle, RADIUS_SERVICE_TYPE, RADIUS_FRAMED);
            }
            // framedprotocol
            if ($status) {
                $status = radius_put_int($handle, RADIUS_FRAMED_PROTOCOL, RADIUS_PPP);
            }
            // framedipaddress
            if ($status) {
                $status = radius_put_addr($handle, RADIUS_FRAMED_IP_ADDRESS, $peer->framed_ip_address);
            }

            if (!$status) {
                Yii::error('RadiusError:' . radius_strerror($handle), static::LOG_CATEGORY);
            }

        } catch (Exception $e) {
            Yii::error(
                'Radius ACCT Exception: '. $e->getMessage().PHP_EOL.$e->getTraceAsString(),
                static::LOG_CATEGORY
            );
        } catch (\Throwable $th) {
            Yii::error(
                'Radius ACCT Throwable: '. $th->getMessage().PHP_EOL.$th->getTraceAsString(),
                static::LOG_CATEGORY
            );
        }

        $request = radius_send_request($handle);
        if ($request) {
            if ($request == RADIUS_ACCOUNTING_RESPONSE) {
                Yii::info('Radius Accounting response: ' . bin2hex($request) . ' Len:' . strlen($request), static::LOG_CATEGORY);
                radius_close($handle);
                return true;
            } else {
                Yii::error('Unexpected Radius Accounting response value:' . $request, static::LOG_CATEGORY);
            }
        } else {
            Yii::error('RadiusError:' . radius_strerror($handle), static::LOG_CATEGORY);
        }
        radius_close($handle);

        return false;
    }

    /**
     * Radius auth request
     *
     * @param Nas $nas
     * @param Peer $peer
     * @return bool
     * @throws Exception
     */
    public function auth(Nas $nas, Peer $peer): bool
    {
        $handle = \radius_auth_open();
        try {
            $status = radius_add_server($handle, $this->server, $this->authPort, $this->secret, $this->timeout, $this->retry);
            if ($status) {
                $status = radius_create_request($handle, RADIUS_ACCESS_REQUEST);
            }
            // nasipaddress
            if ($status) {
                $status = radius_put_addr($handle, RADIUS_NAS_IP_ADDRESS, $nas->ip_address);
            }
            if ($status) {
                $status = radius_put_string($handle, RADIUS_NAS_IDENTIFIER, $nas->endpoint);
            }
            // nasportid
            if ($status) {
                $status = radius_put_int($handle, RADIUS_NAS_PORT, $nas->getConnectPort()); // nas port
            }
            // nasporttype
            if ($status) {
                $status = radius_put_int($handle, RADIUS_NAS_PORT_TYPE, RADIUS_VIRTUAL);
            }
            // servicetype
            if ($status) {
                $status = radius_put_int($handle, RADIUS_SERVICE_TYPE, RADIUS_FRAMED);
            }
            // framedprotocol
            if ($status) {
                $status = radius_put_int($handle, RADIUS_FRAMED_PROTOCOL, RADIUS_PPP);
            }
            // calledstationid
            if ($status) {
                $status = radius_put_string($handle, RADIUS_CALLED_STATION_ID, $nas->ip_address);
            }
            // callingstationid
            if ($status) {
                $status = radius_put_string($handle, RADIUS_CALLING_STATION_ID, $peer->ip_address);
            }
            // username
            if ($status) {
                $status = radius_put_string($handle, RADIUS_USER_NAME, $peer->username);
            }

            // password
            switch ($this->authType) {

                case RadiusAuthTypeHelper::TYPE_CHAP:
                    /* generate Challenge */
                    mt_srand(time());
                    $challenge = mt_rand();
                    // FYI: CHAP = md5(ident + plaintextpass + challenge)
                    $chapValue = pack('H*', md5(pack('Ca*',1, $peer->password . $challenge)));
                    // $chapValue = md5(pack('Ca*', 1, $password . $challenge));
                    // Radius wants the CHAP Ident in the first byte of the CHAP-Password
                    $chapPassword = pack('C', 1) . $chapValue;

                    if ($status) {
                        $status = radius_put_attr($handle, RADIUS_CHAP_PASSWORD, $chapPassword);
                    }
                    if (!$status) {
                        Yii::error('RadiusError: RADIUS_CHAP_PASSWORD: ' . radius_strerror($handle), static::LOG_CATEGORY);
                    }
                    if ($status) {
                        $status = radius_put_attr($handle, RADIUS_CHAP_CHALLENGE, $challenge);
                    }
                    if (!$status) {
                        Yii::error('RadiusError: RADIUS_CHAP_CHALLENGE: ' . radius_strerror($handle), static::LOG_CATEGORY);
                    }
                    break;

                case RadiusAuthTypeHelper::TYPE_MS_CHAPv1:
                    $challenge = MsChap::generateChallenge(8);
                    Yii::info('Challenge: ' . bin2hex($challenge), static::LOG_CATEGORY);
                    if ($status) {
                        $status = radius_put_vendor_attr($handle, RADIUS_VENDOR_MICROSOFT, RADIUS_MICROSOFT_MS_CHAP_CHALLENGE, $challenge);
                    }
                    if (!$status) {
                        Yii::error('RadiusError: RADIUS_MICROSOFT_MS_CHAPv1_CHALLENGE:' . radius_strerror($handle), static::LOG_CATEGORY);
                    }
                    $ntPasswordHash = MsChap::ntPasswordHash($peer->password);
                    $ntResponse = MsChap::challengeResponse($challenge, $ntPasswordHash);
                    $lmResponse = str_repeat ("\0", 24);
                    Yii::info('NT Response: ' . bin2hex($ntResponse), static::LOG_CATEGORY);
                    // Response: chapid, flags (1 = use NT Response), LM Response, NT Response
                    $response = pack('CCa48',1 , 1, $lmResponse . $ntResponse);
                    Yii::info('Response: ' . strlen($response) . ' ' . bin2hex($response), static::LOG_CATEGORY);

                    if ($status) {
                        $status = radius_put_vendor_attr($handle, RADIUS_VENDOR_MICROSOFT, RADIUS_MICROSOFT_MS_CHAP_RESPONSE, $response);
                    }
                    if (!$status) {
                        Yii::error('RadiusError: RADIUS_MICROSOFT_MS_CHAPv1_RESPONSE:' . radius_strerror($handle), static::LOG_CATEGORY);
                    }
                    break;

                case RadiusAuthTypeHelper::TYPE_MS_CHAPv2:
                    $authChallenge = MsChap::generateChallenge(16);
                    Yii::info('Auth Challenge: '.bin2hex($authChallenge), static::LOG_CATEGORY);

                    if ($status) {
                        $status = radius_put_vendor_attr($handle, RADIUS_VENDOR_MICROSOFT, RADIUS_MICROSOFT_MS_CHAP_CHALLENGE, $authChallenge);
                    }
                    if (!$status) {
                        Yii::error('RadiusError: RADIUS_MICROSOFT_MS_CHAPv2_CHALLENGE:' . radius_strerror($handle), static::LOG_CATEGORY);
                    }

                    // we have no client, therefore we generate the Peer-Challenge
                    $peerChallenge = MsChap::generateChallenge(16);
                    Yii::info('Peer Challenge: '.bin2hex($peerChallenge), static::LOG_CATEGORY);

                    $challenge = MsChap::challengeHash($authChallenge, $peerChallenge, $peer->username);
                    $ntPasswordHash = MsChap::ntPasswordHash($peer->password);
                    $ntResponse = MsChap::challengeResponse($challenge, $ntPasswordHash);
                    $reserved = str_repeat ("\0", 8);

                    Yii::info('NT Response: '.bin2hex($ntResponse), static::LOG_CATEGORY);
                    // Response: chapid, flags (1 = use NT Response), Peer challenge, reserved, NT Response
                    $response = pack('CCa16a8a24',1 , 1, $peerChallenge, $reserved, $ntResponse);
                    Yii::info('Response: ' . strlen($response) . ' ' . bin2hex($response), static::LOG_CATEGORY);

                    if ($status) {
                        $status = radius_put_vendor_attr($handle, RADIUS_VENDOR_MICROSOFT, RADIUS_MICROSOFT_MS_CHAP2_RESPONSE, $response);
                    }
                    if (!$status) {
                        Yii::error('RadiusError: RADIUS_MICROSOFT_MS_CHAPv2_RESPONSE:' . radius_strerror($handle), static::LOG_CATEGORY);
                    }
                    break;

                case RadiusAuthTypeHelper::TYPE_PAP:
                    if ($status) {
                        $status = radius_put_string($handle, RADIUS_USER_PASSWORD, $peer->password);
                    }
                    break;

                default:
                    Yii::error('RadiusError: Wrong auth protocol type.', static::LOG_CATEGORY);
                    return false;
            }
            if (!$status) {
                Yii::error('RadiusError:' . radius_strerror($handle), static::LOG_CATEGORY);
            }
        } catch (Exception $e) {
            Yii::error(
                'Radius AUTH Exception: '. $e->getMessage().PHP_EOL.$e->getTraceAsString(),
                static::LOG_CATEGORY
            );
        } catch (\Throwable $th) {
            Yii::error(
                'Radius AUTH Throwable: '. $th->getMessage().PHP_EOL.$th->getTraceAsString(),
                static::LOG_CATEGORY
            );
        }

        $request = radius_send_request($handle);
        if ($request) {
            switch($request) {
                case RADIUS_ACCESS_ACCEPT:
                    Yii::info('Radius Access Request accepted', static::LOG_CATEGORY);
                    $secret = radius_server_secret($handle);
                    if ($secret) {
                        Yii::info('Shared Secret: ' . $secret, static::LOG_CATEGORY);
                        $auth = radius_request_authenticator($handle);
                        if ($auth) {
                            Yii::info('Request Authenticator: ' . bin2hex($auth) . ' Len:' . strlen($auth), static::LOG_CATEGORY);
                            radius_close($handle);
                            return true;
                        } else {
                            Yii::error('RadiusError: ' . radius_strerror($handle), static::LOG_CATEGORY);
                        }
                    } else {
                        Yii::error('RadiusError: ' . radius_strerror($handle), static::LOG_CATEGORY);
                    }
                    break;

                case RADIUS_ACCESS_REJECT:
                    Yii::info('Radius Access Request rejected', static::LOG_CATEGORY);
                    break;

                default:
                    Yii::info('Unexpected Radius Access Request return value', static::LOG_CATEGORY);
            }

            $this->debugAttributes($handle);
        } else {
            Yii::error('RadiusError: ' . radius_strerror($handle), static::LOG_CATEGORY);
        }

        radius_close($handle);

        return false;
    }

    /**
     * @param resource $handle
     * @return false
     * @throws Exception
     */
    private function debugAttributes($handle)
    {
        while ($response = radius_get_attr($handle)) {
            if (!is_array($response)) {
                Yii::error('Error getting attribute: ' . radius_strerror($handle), static::LOG_CATEGORY);
                return false;
            }
            $attr = ArrayHelper::getValue($response, 'attr');
            $data = ArrayHelper::getValue($response, 'data');
            //Yii::info('Got Attr: ' . $attr . ' ' . strlen($data) . ' Bytes : ' . bin2hex($data), static::LOG_CATEGORY);
            switch ($attr) {

                case RADIUS_FRAMED_IP_ADDRESS:
                    $ip = radius_cvt_addr($data);
                    Yii::info('IP: ' . $ip, static::LOG_CATEGORY);
                    break;

                case RADIUS_FRAMED_IP_NETMASK:
                    $mask = radius_cvt_addr($data);
                    Yii::info('MASK: ' . $mask, static::LOG_CATEGORY);
                    break;

                case RADIUS_FRAMED_MTU:
                    $mtu = radius_cvt_int($data);
                    Yii::info('MTU: ' . $mtu, static::LOG_CATEGORY);
                    break;

                case RADIUS_FRAMED_COMPRESSION:
                    $comp = radius_cvt_int($data);
                    Yii::info('Compression: ' . $comp, static::LOG_CATEGORY);
                    break;

                case RADIUS_SESSION_TIMEOUT:
                    $time = radius_cvt_int($data);
                    echo "Session timeout: $time<br>\n";
                    Yii::info('Session timeout: ' . $time, static::LOG_CATEGORY);
                    break;

                case RADIUS_IDLE_TIMEOUT:
                    $idletime = radius_cvt_int($data);
                    Yii::info('Idle timeout: ' . $idletime, static::LOG_CATEGORY);
                    break;

                case RADIUS_SERVICE_TYPE:
                    $type = radius_cvt_int($data);
                    Yii::info('Service Type: ' . $type, static::LOG_CATEGORY);
                    break;

                case RADIUS_CLASS:
                    $radiusClass = radius_cvt_int($data);
                    Yii::info('Class: ' . $radiusClass, static::LOG_CATEGORY);
                    break;

                case RADIUS_FRAMED_PROTOCOL:
                    $proto = radius_cvt_int($data);
                    echo "Protocol: $proto<br>\n";
                    Yii::info('Protocol: ' . $proto, static::LOG_CATEGORY);
                    break;

                case RADIUS_FRAMED_ROUTING:
                    $rout = radius_cvt_int($data);
                    Yii::info('Routing: ' . $rout, static::LOG_CATEGORY);
                    break;

                case RADIUS_FILTER_ID:
                    $id = radius_cvt_string($data);
                    Yii::info('Filter ID: ' . $id, static::LOG_CATEGORY);
                    break;

                case RADIUS_VENDOR_SPECIFIC:
                    $this->debugVendorAttributes($handle, $data);
                    break;
                default:
                    Yii::warning('Unexpected attribute: ' . $attr, static::LOG_CATEGORY);
            }
        }

        return true;
    }

    /**
     * @param resource $handle
     * @param array $vendorData
     * @return bool
     * @throws Exception
     */
    private function debugVendorAttributes($handle, $vendorData)
    {;
        $response = radius_get_vendor_attr($vendorData);
        if (is_array($response)) {
            $vendor = ArrayHelper::getValue($response, 'vendor');
            $attr = ArrayHelper::getValue($response, 'attr');
            $data = ArrayHelper::getValue($response, 'data');
            if ($vendor == RADIUS_VENDOR_MICROSOFT) {
                switch ($attr) {
                    case RADIUS_MICROSOFT_MS_CHAP2_SUCCESS:
                        $mschap2resp = radius_cvt_string($data);
                        Yii::info('MS CHAPv2 success: ' . $mschap2resp, static::LOG_CATEGORY);
                        break;

                    case RADIUS_MICROSOFT_MS_CHAP_ERROR:
                        $errormsg = radius_cvt_string(substr($data,1));
                        Yii::info('MS CHAP Error: ' . $errormsg, static::LOG_CATEGORY);
                        break;

                    case RADIUS_MICROSOFT_MS_CHAP_DOMAIN:
                        $domain = radius_cvt_string($data);
                        echo "MS CHAP Domain: $domain<br>\n";
                        Yii::info('MS CHAP Domain: ' . $domain, static::LOG_CATEGORY);
                        break;

                    case RADIUS_MICROSOFT_MS_MPPE_ENCRYPTION_POLICY:
                        $policy = radius_cvt_int($data);
                        Yii::info('MS MPPE Policy: ' . $policy, static::LOG_CATEGORY);
                        break;

                    case RADIUS_MICROSOFT_MS_MPPE_ENCRYPTION_TYPES:
                        $type = radius_cvt_int($data);
                        Yii::info('MS MPPE Type: ' . $type, static::LOG_CATEGORY);
                        break;

                    case RADIUS_MICROSOFT_MS_CHAP_MPPE_KEYS:
                        $demangled = radius_demangle($handle, $data);
                        $lmKey = substr($demangled, 0, 8);
                        $ntKey = substr($demangled, 8, RADIUS_MPPE_KEY_LEN);
                        Yii::info('MS MPPE Keys: LM-Key: ' . bin2hex($lmKey) . ' NT-Key: ' . bin2hex($ntKey), static::LOG_CATEGORY);
                        break;

                    case RADIUS_MICROSOFT_MS_MPPE_SEND_KEY:
                        $demangled = radius_demangle_mppe_key($handle, $data);
                        Yii::info('MS MPPE Send Key: ' . bin2hex($demangled), static::LOG_CATEGORY);
                        break;

                    case RADIUS_MICROSOFT_MS_MPPE_RECV_KEY:
                        $demangled = radius_demangle_mppe_key($handle, $data);
                        Yii::info('MS MPPE Receive Key: ' . bin2hex($demangled), static::LOG_CATEGORY);
                        break;

                    case RADIUS_MICROSOFT_MS_PRIMARY_DNS_SERVER:
                        $server = radius_cvt_string($data);
                        Yii::info('MS Primary DNS Server: ' . $server, static::LOG_CATEGORY);
                        break;

                    default:
                        Yii::info('Unexpected Microsoft attribute: ' . $attr, static::LOG_CATEGORY);
                }
            } else {
                Yii::warning('Unexpected Vendor: ' . radius_strerror($handle), static::LOG_CATEGORY);
            }
        } else {
            Yii::warning('Error getting Vendor attribute: ' . radius_strerror($handle), static::LOG_CATEGORY);
        }

        return true;
    }
}
