<?php

namespace app\models;

use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class RestController
 */
class RestController extends Controller
{
    /**
     * @inheritdoc
     */
    public $serializer = 'app\models\ApiSerializer';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        if (YII_DEBUG) {
            /**
             *  Accepts POST requests from editor.swagger.io
             */
            $behaviors['corsFilter'] = [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600,
                ],
            ];
        }
        unset($behaviors['rateLimiter'], $behaviors['authenticator']);

        $behaviors['contentNegotiator']['formats'] = ['application/json' => Response::FORMAT_JSON, ];

        return $behaviors;
    }

    /**
     * @param mixed $data
     * @param string $message
     * @param string $code
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    protected function serializeData($data, $message = '', $code = '')
    {
        return \Yii::createObject($this->serializer)->serialize($data, $message, $code);
    }
}
