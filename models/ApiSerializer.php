<?php

namespace app\models;

use yii\base\Arrayable;
use yii\base\Model;
use yii\data\DataProviderInterface;
use yii\rest\Serializer;
use yii\web\UnprocessableEntityHttpException;

/**
 * Class ApiSerializer
 * @package frontend\modules\api\models
 */
class ApiSerializer extends Serializer
{
    public $feedTokenHeader = 'X-Api-Feed-Token';
    public $feedItemsCountHeader = 'X-Api-Feed-Items-Count';
    public $feedLimitHeader = 'X-Api-Feed-Limit';


    /**
     * @param mixed $data
     * @param string $message
     * @param string $code
     * @return array|mixed
     * @throws UnprocessableEntityHttpException
     */
    public function serialize($data, $message = '', $code = '')
    {
        if ($data instanceof Model && $data->hasErrors()) {
            return $this->serializeModelErrors($data, $message, $code);
        } elseif ($data instanceof Arrayable) {
            return $this->serializeModel($data);
        } elseif ($data instanceof DataProviderInterface) {
            return $this->serializeDataProvider($data);
        } else {
            return $data;
        }
    }

    /**
     * @param Model $model
     * @param string $message
     * @param string $code
     * @throws UnprocessableEntityHttpException
     * @return mixed|string
     */
    protected function serializeModelErrors($model, $message = '', $code = '')
    {
        $message = (empty($message)) ? 'Data Validation Failed.' : $message;

        //$errors = $model->getFirstErrors();
        $errors = $model->getErrors();
        if (!empty($errors)) {
            $this->response->setStatusCode(422, 'Data Validation Failed.');
            $result = [];
            foreach ($errors as $name => $fieldMessages) {
                foreach ($fieldMessages as $message) {
                    $result[] = [
                        'name' => $name,
                        'message' => $message,
                    ];
                }
            }

            return $result;
        }
        throw new UnprocessableEntityHttpException($message, $code);
    }
}
