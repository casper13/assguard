<?php

namespace app\models;

use app\services\AddressService;
use Exception;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * NAS model
 *
 * @property string $id
 * @property string $public_key
 * @property string $endpoint
 * @property int $port
 * @property string $ip_address
 * @property string $framed_ip_address
 * @property string $nic
 * @property array $dns
 */
class Nas extends Model
{
    const PORTS_LIST = 'ports';

    const WG_CMD = '/usr/bin/sudo /usr/bin/wg';

    public $public_key;
    public $endpoint;
    public $port;
    public $ip_address;
    public $framed_ip_address;
    public $nic;
    public $dns;
    private $connectPort;
    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        $this->public_key = ArrayHelper::getValue(Yii::$app->params, 'wg.public_key');
        $this->endpoint = ArrayHelper::getValue(Yii::$app->params, 'wg.endpoint');
        $this->port = ArrayHelper::getValue(Yii::$app->params, 'wg.port');
        $this->ip_address = ArrayHelper::getValue(Yii::$app->params, 'wg.ip_address');
        $this->nic = ArrayHelper::getValue(Yii::$app->params, 'wg.nic');
        $this->framed_ip_address = ArrayHelper::getValue(Yii::$app->params, 'wg.framed_ip_address');
        $this->dns = (array)ArrayHelper::getValue(Yii::$app->params, 'wg.dns', ['1.1.1.1', '1.1.0.0',]);
    }

    /**
     * @return string
     */
    public function getPublicKey(): string
    {
        if ($this->public_key === null) {
            $command = self::WG_CMD.' show '.$this->nic.' public-key';
            unset($out, $code);
            exec($command, $out, $code);
            $this->public_key = reset($out);
            if ($code != 0) {
                Yii::error(
                    'Command: '.$command.PHP_EOL.
                    'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                    'Command output: '.VarDumper::dumpAsString($out).PHP_EOL.
                    'public_key = "'.$this->public_key.'"'
                );
            }
        }

        return $this->public_key;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        if ($this->ip_address === null) {
            $this->ip_address = (string)$_SERVER['SERVER_ADDR'];
        }

        return $this->ip_address;
    }

    /**
     * @return string
     */
    public function getFramedIpAddress(): string
    {
        if ($this->framed_ip_address === null) {
            $service = new AddressService();
            $this->framed_ip_address = $service->getServerIp();
        }

        return $this->framed_ip_address;
    }

    /**
     * @return string
     */
    public function getEndpoint(): string
    {
        if ($this->endpoint === null) {
            $this->endpoint = $this->getIpAddress();
        }

        return $this->endpoint;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        if ($this->port === null) {
            $command = self::WG_CMD.' show '.$this->nic.' listen-port';
            unset($out, $code);
            exec($command, $out, $code);
            $this->port = reset($out);
            if ($code != 0) {
                Yii::error(
                    'Command: '.$command.PHP_EOL.
                    'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                    'Command output: '.VarDumper::dumpAsString($out).PHP_EOL.
                    'port = "'.$this->port.'"'
                );
            }
        }

        return $this->port;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getDns(): array
    {
        if ($this->dns === null) {
            $this->dns = (array)ArrayHelper::getValue(Yii::$app->params, 'wg.dns', ['1.1.1.1', '1.1.0.0',]);
        }

        return $this->dns;
    }

    /**
     * @param Peer $peer
     * @return int
     */
    public function addPeer(Peer $peer): int
    {
        $command = self::WG_CMD.' set '.$this->nic.' peer '.escapeshellarg($peer->public_key).' allowed-ips '.escapeshellarg($peer->framed_ip_address.'/32');
        unset($out, $code);
        exec($command, $out, $code);
        if ($code != 0) {
            Yii::error(
                'Command: '.$command.PHP_EOL.
                'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                'Command output: '.VarDumper::dumpAsString($out)
            );
        } else {
            Yii::info(
                'Add peer successfully.'.PHP_EOL.
                'Command: '.$command.PHP_EOL.
                'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                'Command output: '.VarDumper::dumpAsString($out)
            );
            if (1) {
            $postCmd = '/opt/con.sh';
            if (file_exists($postCmd) && is_executable($postCmd)) {
                $command = '/usr/bin/sudo '.$postCmd.' "'.$this->ip_address.'" "'.$this->framed_ip_address.'" "wg" "'.$peer->username.'" "'.$peer->ip_address.'" "'.$peer->framed_ip_address.'"';
                unset($out, $postCode);
                exec($command, $out, $postCode);
                if ($postCode != 0) {
                    Yii::error(
                        'Command: '.$command.PHP_EOL.
                        'Command exit code: '.VarDumper::dumpAsString($postCode).PHP_EOL.
                        'Command output: '.VarDumper::dumpAsString($out)
                    );
                } else {
                    Yii::info(
                        'Run post-up script successfully.'.PHP_EOL.
                        'Command: '.$command.PHP_EOL.
                        'Command exit code: '.VarDumper::dumpAsString($postCode).PHP_EOL.
                        'Command output: '.VarDumper::dumpAsString($out)
                    );
                }
            }
            }
        }

        return $code;
    }

    /**
     * @param Peer $peer
     * @return int
     */
    public function removePeer(Peer $peer): int
    {
        $command = self::WG_CMD.' set '.$this->nic.' peer '.escapeshellarg($peer->public_key).' remove';
        exec($command, $out, $code);
        if ($code != 0) {
            Yii::error(
                'Command: '.$command.PHP_EOL.
                'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                'Command output: '.VarDumper::dumpAsString($out)
            );
        } else {
            Yii::info(
                'Remove peer successfully.'.PHP_EOL.
                'Command: '.$command.PHP_EOL.
                'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                'Command output: '.VarDumper::dumpAsString($out)
            );
            if (1) {
            $postCmd = '/opt/discon.sh';
            if (file_exists($postCmd) && is_executable($postCmd)) {
                $command = '/usr/bin/sudo '.$postCmd.' "'.$this->ip_address.'" "'.$this->framed_ip_address.'" "wg" "'.$peer->username.'" "'.$peer->ip_address.'" "'.$peer->framed_ip_address.'"';
                unset($out, $postCode);
                exec($command, $out, $postCode);
                if ($postCode != 0) {
                    Yii::error(
                        'Command: '.$command.PHP_EOL.
                        'Command exit code: '.VarDumper::dumpAsString($postCode).PHP_EOL.
                        'Command output: '.VarDumper::dumpAsString($out)
                    );
                } else {
                    Yii::info(
                        'Run post-up script successfully.'.PHP_EOL.
                        'Command: '.$command.PHP_EOL.
                        'Command exit code: '.VarDumper::dumpAsString($postCode).PHP_EOL.
                        'Command output: '.VarDumper::dumpAsString($out)
                    );
                }
            }
            }
        }

        return  $code;
    }

    /**
     * Update peer transfer statistics: input_octets and output_octets octets
     *
     * @param Peer $peer
     * @return bool
     * @throws InvalidConfigException
     */
    public function updateStats(Peer &$peer): bool
    {
        // Get $inputOctets, $outputOctets stats for accounting
        $command = self::WG_CMD.' show '.$this->nic.' dump |grep "'.$peer->public_key.'"' ;
        exec($command, $out, $code);
        if ($code != 0) {
            Yii::error(
                'Command: '.$command.PHP_EOL.
                'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                'Command output: '.VarDumper::dumpAsString($out)
            );
            return false;
        } else {
            Yii::info(
                'Dump peer successfully.'.PHP_EOL.
                'Command: '.$command.PHP_EOL.
                'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                'Command output: '.VarDumper::dumpAsString($out)
            );
        }
        foreach ($out as $line) {
            if (strpos($line, $peer->public_key) !== false) {
                //[$pubKey, $preSharedKey, $endpoint, $framedIpAddress, $latestHandshakes, $inputOctets, $outputOctets, $persistentKeepalive] = preg_split("/[\s]+/", $line);
                $splits = preg_split("/[\s]+/", $line);
                if (count($splits) < 7) {
                    $inputOctets = $outputOctets = $latestHandshakes = 0;
                } else {
                    Yii::info('Dump:'.PHP_EOL.VarDumper::dumpAsString($splits));
                    $pubKey = ArrayHelper::getValue($splits, 0);
                    $preSharedKey = ArrayHelper::getValue($splits, 1);
                    $endpoint = ArrayHelper::getValue($splits, 2);
                    $framedIpAddress = ArrayHelper::getValue($splits, 3);
                    $latestHandshakes = ArrayHelper::getValue($splits, 4);
                    $inputOctets = ArrayHelper::getValue($splits, 5);
                    $outputOctets = ArrayHelper::getValue($splits, 6);
                    $persistentKeepalive = ArrayHelper::getValue($splits, 7);
                    \Yii::info(
                        'SessionID#'.$peer->session_id.':'.PHP_EOL.
                        'Public key = "'.$pubKey.'"'.PHP_EOL.
                        'Pre Shared key = "'.$preSharedKey.'"'.PHP_EOL.
                        'Endpoint = "'.$endpoint.'"'.PHP_EOL.
                        'Framed IP Address = "'.$framedIpAddress.'"'.PHP_EOL.
                        'Latest Handshakes = "'.$latestHandshakes.'" | '.Yii::$app->formatter->asDatetime($latestHandshakes).PHP_EOL.
                        'inputOctets = '.$inputOctets.PHP_EOL.
                        'outputOctets = '.$outputOctets.PHP_EOL.
                        'Persistent Keepalive = '.$persistentKeepalive
                    );
                }
                if (!empty($latestHandshakes)) {
                    $peer->latest_handshakes_at = (int)$latestHandshakes;
                }
                if (!empty($inputOctets)) {
                    $peer->input_octets = $inputOctets;
                }
                if (!empty($outputOctets)) {
                    $peer->output_octets = $outputOctets;
                }
            }
        }

        return true;
    }

    /**
     * @return array|false
     * @throws InvalidConfigException
     */
    public function getConfigs()
    {
        // Get $inputOctets, $outputOctets stats for accounting
        $command = self::WG_CMD.' show '.$this->nic.' dump' ;
        exec($command, $out, $code);
        if ($code != 0) {
            Yii::error(
                'Command: '.$command.PHP_EOL.
                'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                'Command output: '.VarDumper::dumpAsString($out)
            );
            return false;
        } else {
            Yii::info(
                'Dump peer successfully.'.PHP_EOL.
                'Command: '.$command.PHP_EOL.
                'Command exit code: '.VarDumper::dumpAsString($code).PHP_EOL.
                'Command output: '.PHP_EOL.VarDumper::dumpAsString($out)
            );
        }
        $list = [];
        foreach ($out as $line) {
            //[$pubKey, $preSharedKey, $endpoint, $framedIpAddress, $latestHandshakes, $inputOctets, $outputOctets, $persistentKeepalive] = preg_split("/[\s]+/", $line);
            $splits = preg_split("/[\s]+/", $line);
            if (count($splits) > 4) {
                Yii::info('Dump:'.PHP_EOL.VarDumper::dumpAsString($splits));
                $item = [
                    'pubKey' => ArrayHelper::getValue($splits, 0),
                    'preSharedKey' => ArrayHelper::getValue($splits, 1),
                    'endpoint' => ArrayHelper::getValue($splits, 2),
                    'framedIpAddress' => ArrayHelper::getValue($splits, 3),
                    'latestHandshakes' => ArrayHelper::getValue($splits, 4),
                    'inputOctets' => ArrayHelper::getValue($splits, 5),
                    'outputOctets' => ArrayHelper::getValue($splits, 6),
                    'persistentKeepalive' => ArrayHelper::getValue($splits, 7),
                ];
                $list[] = $item;
                \Yii::info(
                    'Public key = "'.$item['pubKey'].'"'.PHP_EOL.
                    'Pre Shared key = "'.$item['preSharedKey'].'"'.PHP_EOL.
                    'Endpoint = "'.$item['endpoint'].'"'.PHP_EOL.
                    'Framed IP Address = "'.$item['framedIpAddress'].'"'.PHP_EOL.
                    'Latest Handshakes = "'.$item['latestHandshakes'].'" | '.Yii::$app->formatter->asDatetime($item['latestHandshakes']).PHP_EOL.
                    'inputOctets = '.$item['inputOctets'].PHP_EOL.
                    'outputOctets = '.$item['outputOctets'].PHP_EOL.
                    'Persistent Keepalive = '.$item['persistentKeepalive']
                );
            }
        }

        return $list;
    }

    /**
     * Get random port from REDIS ports list
     * @return int|null
     */
    private function getRedisPort()
    {
        $port = null;
        $list = Yii::$app->redis->lrange(self::PORTS_LIST, 0, -1);
        if (!empty($list)) {
            $list = array_filter($list, 'is_numeric');
            $list = array_filter(
                $list,
                function ($item) {
                    return $item >= 1 && $item <= 65535;
                }
            );
            if (!empty($list)) {
                // Get random port from list
                $port = $list[array_rand($list)];
            }
        }

        return $port;
    }

    /**
     * Get port from port range or default port as fallback
     * @return int
     */
    public function getConnectPort()
    {
        if ($this->connectPort === null) {
            // get port from redis
            $port = $this->getRedisPort();

            if (empty($port)) {
                // Fallback: get port from app config
                $port = $this->port;
            }

            if (empty($port)) {
                // Fallback: get port from wireguard runtime directly
                $port = $this->getPort();
            }

            if (!empty($port)) {
                $this->connectPort = $port;
            }
        }

        return (int)$this->connectPort;
    }
}
