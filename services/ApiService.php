<?php

namespace app\services;

use app\models\Nas;
use app\models\Peer;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

class ApiService
{
    public const PROTO = 'wg';

    /**
     * @param Nas $nas
     * @param Peer $peer
     * @param int $time
     * @return bool
     * @throws Exception
     */
    public function connect(Nas $nas, Peer $peer, int $time): bool
    {
        $data = [
            'timestamp' => $time,
            'server_ip' => $nas->ip_address,
            'protocol' => self::PROTO,
            'username' => $peer->username,
            'user_ip' => $peer->ip_address,
        ];
        $url = (string)ArrayHelper::getValue(Yii::$app->params, 'postauth.connect');
        $status = $this->post($url, $data);
        if (!$status) {
            $url = (string)ArrayHelper::getValue(Yii::$app->params, 'lajupostauth.connect');
            $status = $this->post($url, $data);
        }

        return $status;
    }

    /**
     * @param Nas $nas
     * @param Peer $peer
     * @param int $time
     * @return bool
     * @throws Exception
     */
    public function disconnect(Nas $nas, Peer $peer, int $time): bool
    {
        $data = [
            'timestamp' => $time,
            'server_ip' => $nas->ip_address,
            'protocol' => self::PROTO,
            'username' => $peer->username,
            'user_ip' => $peer->ip_address,
        ];
        $url = (string)ArrayHelper::getValue(Yii::$app->params, 'postauth.disconnect');

        $status = $this->post($url, $data);
        if (!$status) {
            $url = (string)ArrayHelper::getValue(Yii::$app->params, 'lajupostauth.disconnect');
            $status = $this->post($url, $data);
        }

        return $status;
    }

    /**
     * @param string $url
     * @param array $data
     * @return bool
     */
    protected function post(string $url, array $data): bool
    {
        $opts = [
            'http' => [
                'method'  => 'POST',
                'header'  => 'Content-type: application/json',
                'content' => Json::encode($data),
                'timeout' => (int)ArrayHelper::getValue(Yii::$app->params, 'postauth.timeout', 30),
                "ignore_errors" => true,
            ],
        ];
        $context  = stream_context_create($opts);

        $response = file_get_contents($url, false, $context);
        Yii::info('API endpoint "'.$url.'" request'.PHP_EOL.'Response: '.PHP_EOL.$response.PHP_EOL.' Headers: '.PHP_EOL.VarDumper::dumpAsString($http_response_header));
        $status_line = ArrayHelper::getValue($http_response_header, 0);

        preg_match('{HTTP/\S*\s(\d{3})}', $status_line, $match);

        $status = ArrayHelper::getValue($match, 1);

        if ($status == 418) {
            return false;
        }

        return true;
    }
}
