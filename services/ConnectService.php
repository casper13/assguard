<?php


namespace app\services;


use app\helpers\RadiusAcctStatusTypeHelper;
use app\helpers\RadiusAcctTerminateCauseType;
use app\models\ApiRequest;
use app\models\ConnectResponse;
use app\models\DisconnectResponse;
use app\models\Nas;
use app\models\Peer;
use app\models\Radius;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\console\ExitCode;
use yii\db\Exception as DbException;
use yii\db\StaleObjectException;

class ConnectService
{
    /**
     * @param ApiRequest $model
     * @param int $time
     * @param string $ipAddress
     * @return ConnectResponse|null
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function connect($model, int $time, string $ipAddress)
    {
        if (!$model->validate()) {
            return null;
        }
        // Delete all previous client config on connect

        // By WireGuard PublicKey
        /** @var Peer|null $peer */
        $peer = Peer::findByPublicKey($model->publicKey);
        if (!empty($peer) && $peer->username != $model->username) {
            $this->disconnect($model, $time, $ipAddress);
            unset($peer);
        }
        if (empty($peer)) {
            $peer = new Peer();
            $peer->generateSessionId($time, 16);
            $peer->username = $model->username;
            $peer->password = $model->password;
            $peer->public_key = $model->publicKey;
            $peer->ip_address = $ipAddress;
            $peer->acquireFramedIp();
            $peer->input_octets = 0;
            $peer->output_octets = 0;
            $peer->connected_at = $time;
            $peer->accounted_at = $time;
            $peer->latest_handshakes_at = $time;
        }
        $nas = new Nas();

        $radiusClient = new Radius();
        if ($radiusClient->isEnabled()) {
            $auth = $radiusClient->auth($nas, $peer);
            if (!$auth) {
                $message = 'Incorrect username or password.';
                $model->addError('username', $message);
                $model->addError('password', $message);
                return null;
            }
        }
        $api = new ApiService();
        $apiAuth = $api->connect($nas, $peer, $time);
        if (!$apiAuth) {
            $message = 'Incorrect username or password.';
            $model->addError('username', $message);
            $model->addError('password', $message);
            return null;
        }
        if ($nas->addPeer($peer) != ExitCode::OK) {
            $message = 'Error happened when importing public key';
            $model->addError('username', $message);
            $model->addError('password', $message);
            $model->addError('publicKey', $message);
            return null;
        }

        if ($peer->getIsNewRecord()) {
            $radiusAcctStatusType = RadiusAcctStatusTypeHelper::TYPE_RADIUS_START;
        } else {
            $radiusAcctStatusType = RadiusAcctStatusTypeHelper::TYPE_RADIUS_ALIVE;
            $nas->updateStats($peer);
        }

        $peer->save();

        if ($radiusClient->isEnabled()) {
            $acctStart = $radiusClient->acct($nas, $peer, $time, $radiusAcctStatusType);
            if ($acctStart) {
                $peer->accounted_at = $time;
            }
        }

        $connectResponse = new ConnectResponse();
        $connectResponse->publicKey = $nas->public_key;
        $connectResponse->endpoint = $nas->endpoint;
        $connectResponse->port = $nas->getConnectPort();
        $connectResponse->dns = $nas->dns;
        $connectResponse->address = $peer->getCidrIpAddress();

        return $connectResponse;
    }

    /**
     * @param ApiRequest $model
     * @param int $time
     * @param string $ipAddress
     * @return DisconnectResponse|null
     * @throws InvalidConfigException
     * @throws DbException
     * @throws StaleObjectException
     */
    public function disconnect($model, $time, string $ipAddress)
    {
        if (!$model->validate()) {
            return null;
        }
        $disconnectResponse = new DisconnectResponse();
        $disconnectResponse->username = (string)$model->username;
        $disconnectResponse->tx = 0;
        $disconnectResponse->rx = 0;
        $peer = Peer::findByPublicKey($model->publicKey);
        if (!empty($peer)) {
            $nas = new Nas();
            $nas->updateStats($peer);
            $disconnectResponse->username = $peer->username;
            $disconnectResponse->rx = $peer->input_octets;
            $disconnectResponse->tx = $peer->output_octets;

            $radiusClient = new Radius();
            if ($radiusClient->isEnabled()) {
                $radiusAcctStatusType = RadiusAcctStatusTypeHelper::TYPE_RADIUS_STOP;
                $terminateCauseType = RadiusAcctTerminateCauseType::TYPE_RADIUS_TERM_USER_REQUEST;
                $acctStop = $radiusClient->acct($nas, $peer, $time, $radiusAcctStatusType, $terminateCauseType);
                if ($acctStop) {
                    $peer->accounted_at = $time;
                }
            }

            if ($nas->removePeer($peer) != ExitCode::OK) {
                $message = 'Error happened when removing public key';
                $model->addError('username', $message);
                $model->addError('publicKey', $message);
                //return null;
            }

            $api = new ApiService();
            $apiAuth = $api->disconnect($nas, $peer, $time);

            $peer->delete();

        }

        return $disconnectResponse;
    }
}
