<?php

namespace app\services;

use Exception;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class AddressService
 * @package app\services
 */
class AddressService
{
    /**
     * @var string
     */
    private $subnet;
    /**
     * @var int|null
     */
    private $cidr = null;
    /**
     * @var int|null
     */
    private $serverIp = null;

    /**
     * @var int|null
     */
    private $firstIp = null;
    /**
     * @var int|null
     */
    private $lastIp = null;

    /**
     * AddressService constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->subnet = (string)ArrayHelper::getValue(Yii::$app->params, 'wg.subnet', '10.2.0.0/16');
    }

    /**
     * @return int
     */
    public function getFirstIp(): int
    {
        if ($this->firstIp === null) {
            $list = explode('/', $this->subnet);
            $this->firstIp = (ip2long($list[0])) & ((-1 << (32 - (int)$list[1])));
        }

        return $this->firstIp;
    }

    /**
     * @return int
     */
    public function getLastIp(): int
    {
        if ($this->lastIp === null) {
            $list = explode('/', $this->subnet);
            $this->lastIp = $this->getFirstIp() + pow(2, (32 - (int)$list[1])) - 1;
        }

        return $this->lastIp;
    }

    /**
     * @return int
     */
    public function getCidr(): int
    {
        if ($this->cidr === null) {
            $list = explode('/', $this->subnet);
            $this->cidr = (int)$list[1];
        }

        return (int)$this->cidr;
    }

    /**
     * @return string
     */
    public function getServerIp()
    {
        if ($this->serverIp === null) {
            $firstIp = $this->getFirstIp();
            $this->serverIp = long2ip($firstIp + 1);
        }

        return $this->serverIp;
    }
}
