# Setup

```shell
sudo apt update
sudo apt upgrade -y
cd /opt
sudo git clone https://github.com/exploitfate/cfip.git
sudo chmod +x /opt/cfip/update.sh
sudo apt autoremove
apt-get install -y php-curl php-cli php-intl php-fpm php-mbstring php-xml php-zip php-bcmath php-redis php-radius curl git htop iotop zip unzip nginx redis-server php-xdebug
apt-get install -y wireguard ntp
sudo /opt/cfip/update.sh
sudo apt install python3-certbot-nginx certbot
sudo usermod www-data -s /bin/bash
sudo cp /etc/skel/.* /var/www/
sudo chown -R www-data: /var/www/
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | sudo tee /etc/apt/sources.list.d/newrelic.list
wget -O- https://download.newrelic.com/548C16BF.gpg | sudo apt-key add -
sudo apt update
sudo apt -y install newrelic-php5
sudo service php7.4-fpm restart
echo 'cron.*                         /var/log/cron.log
& stop
' | sudo tee /etc/rsyslog.d/22-cron.conf > /dev/null 2>&1

echo '
#Specify a charset
charset utf-8;

client_max_body_size 128M;
server_tokens off;

merge_slashes off;
' | sudo tee /etc/nginx/conf.d/charset.conf > /dev/null 2>&1

echo '
# Gzip Settings
#gzip on;               # enabled by default
#gzip_disable "msie6";  # enabled by default
gzip_vary on;
gzip_proxied any;
gzip_comp_level 6;
gzip_buffers 16 8k;
gzip_types
    application/atom+xml
    application/javascript
    application/x-javascript
    application/json
    application/ld+json
    application/manifest+json
    application/rss+xml
    application/vnd.geo+json
    application/vnd.ms-fontobject
    application/x-font-ttf
    application/x-web-app-manifest+json
    application/xhtml+xml
    application/xml
    font/opentype
    image/bmp
    image/svg+xml
    image/x-icon
    text/cache-manifest
    text/css
    text/plain
    text/vcard
    text/vnd.rim.location.xloc
    text/vtt
    text/x-component
    text/x-cross-domain-policy;
' | sudo tee /etc/nginx/conf.d/gzip.conf > /dev/null 2>&1

echo '
# Cross domain webfont access
location ~* \.(?:ttf|ttc|otf|eot|woff|woff2)$ {
    # Also, set cache rules for webfonts.
    #
    # See http://wiki.nginx.org/HttpCoreModule#location
    # And https://github.com/h5bp/server-configs/issues/85
    # And https://github.com/h5bp/server-configs/issues/86
    expires 1M;
    access_log off;
    add_header Cache-Control "public";
}
' | sudo tee /etc/nginx/cross-domain-fonts.conf > /dev/null 2>&1

echo '
# Expire rules for static content

# No default expire rule. This config mirrors that of apache as outlined in the
# html5-boilerplate .htaccess file. However, nginx applies rules by location,
# the apache rules are defined by type. A consequence of this difference is that
# if you use no file extension in the url and serve html, with apache you get an
# expire time of 0s, with nginx you would get an expire header of one month in the
# future (if the default expire rule is 1 month). Therefore, do not use a
# default expire rule with nginx unless your site is completely static

# cache.appcache, your document html and data
location ~* \.(?:manifest|appcache|html?|xml|json)$ {
  expires -1;
  #access_log logs/static.log;
}

# Feed
location ~* \.(?:rss|atom)$ {
  expires 1h;
  add_header Cache-Control "public";
}

# Media: images, icons, video, audio, HTC
location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc|webp|mp3)$ {
  expires 1M;
  access_log off;
  add_header Cache-Control "public";
}

# CSS and Javascript
location ~* \.(?:css|js)$ {
  expires 1y;
  access_log off;
  add_header Cache-Control "public";
}
# favicon disable logs
location = /favicon.ico {
  access_log off;
  log_not_found off;
}
' | sudo tee /etc/nginx/expires.conf > /dev/null 2>&1

echo '
# Prevent clients from accessing hidden files (starting with a dot)
# This is particularly important if you store .htpasswd files in the site hierarchy
location /.well-known {
    auth_basic off;
    allow all;
}

location ~* (?:^|/)\. {
    deny all;
}

# Prevent clients from accessing to backup/config/source files
location ~* (?:\.(?:bak|config|sql|fla|psd|ini|log|sh|inc|swp|dist|md)|~)$ {
    deny all;
}
' | sudo tee /etc/nginx/protect-system-files.conf > /dev/null 2>&1

echo 'mod:$apr1$tqpHgcIo$GNAMB2UPUaHVV9XsV38xz0
' | sudo tee /var/www/.htpasswd > /dev/null 2>&1

echo '
##
# You should look at the following URL s in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# https://www.nginx.com/resources/wiki/start/
# https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/
# https://wiki.debian.org/Nginx/DirectoryStructure
#
# In most cases, administrators will remove this file from sites-enabled/ and
# leave it as reference inside of sites-available where it will continue to be
# updated by the nginx packaging team.
#
# This file will automatically load configuration files provided by other
# applications, such as Drupal or Wordpress. These applications will be made
# available underneath a path with that package name, such as /drupal8.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

# Default server configuration
#

# www
server {
        listen       80;
        server_name  www.domain.com;
        return 301   $scheme://domain.com$request_uri;
}

server {
        server_name domain.com;

        listen 80;
        listen [::]:80;

        #auth_basic "Restricted";
        #auth_basic_user_file /var/www/.htpasswd;

        root /var/www/domain.com/html/frontend/web;

        access_log /var/www/domain.com/log/access.log combined; # buffer=50k;
        error_log /var/www/domain.com/log/error.log notice;

        # Add index.php to the list if you are using PHP
        index index.php index.html index.htm index.nginx-debian.html;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                # try_files $uri $uri/ =404;
                try_files $uri $uri/ /index.php$is_args$args;
        }

#        if ($http_user_agent ~* "Chrome/43.0.2357.81") {
#                rewrite ^ http://www.google.com/ permanent;
#        }
        location /images {
                auth_basic off;
        }
        # pass PHP scripts to FastCGI server
        #
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;

                # With php-fpm (or other unix sockets):
                fastcgi_pass unix:/var/run/php/php-fpm.sock;
                # With php-cgi (or other tcp sockets):
                #fastcgi_pass 127.0.0.1:9000;
        }

        include cross-domain-fonts.conf;
        include protect-system-files.conf;
        include expires.conf;
}

server {
        listen       443 ssl;
        server_name  www.domain.com;
        ssl_certificate /etc/letsencrypt/live/domain.com/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/domain.com/privkey.pem; # managed by Certbot

        return 301   $scheme://domain.com$request_uri;

}

server {
        server_name domain.com;

        # SSL configuration
        #
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        ssl_certificate /etc/letsencrypt/live/domain.com/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/domain.com/privkey.pem; # managed by Certbot
#        listen [::]:443 ssl ipv6only=on; # managed by Certbot
#        listen 443 ssl; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

        #auth_basic "Restricted";
        #auth_basic_user_file /var/www/.htpasswd;

        root /var/www/domain.com/html/frontend/web;

        access_log /var/www/domain.com/log/access.log combined; # buffer=50k;
        error_log /var/www/domain.com/log/error.log notice;

        # Add index.php to the list if you are using PHP
        index index.php index.html index.htm index.nginx-debian.html;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                # try_files $uri $uri/ =404;
                try_files $uri $uri/ /index.php$is_args$args;
        }

        location /images {
                auth_basic off;
        }
        # pass PHP scripts to FastCGI server
        #
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;

                # With php-fpm (or other unix sockets):
                fastcgi_pass unix:/var/run/php/php-fpm.sock;
                # With php-cgi (or other tcp sockets):
                #fastcgi_pass 127.0.0.1:9000;
        }

        include cross-domain-fonts.conf;
        include protect-system-files.conf;
        include expires.conf;

}
' | sudo tee /etc/nginx/sites-available/domain.com > /dev/null 2>&1

cd /etc/nginx/sites-enabled/

sudo ln -s ../sites-enabled/domain.com .

#sudo -S -u www-data -i /bin/bash -l -c "git clone git@repo.url /var/www/domain.com/html"

sudo sed -i "s#/var/log/nginx#/var/www/domain.com/log/*.log\n/var/log/nginx#g" /etc/logrotate.d/nginx

sudo -S -u www-data -i /bin/bash -l -c "git -C /var/www/domain.com/html checkout master"

sudo -S -u www-data -i /bin/bash -l -c "git -C /var/www/domain.com/html pull"

sudo -S -u www-data -i /bin/bash -l -c "composer i -o -d /var/www/domain.com/html"
```

echo '
www-data ALL=(ALL) NOPASSWD: /usr/bin/wg, /usr/bin/wg-quick, /etc/wireguard
' | sudo tee /etc/sudoers.d/01-wg > /dev/null 2>&1

# Generate WireGuard key pair
wg genkey | sudo tee /etc/wireguard/ass-privatekey | wg pubkey | sudo tee /etc/wireguard/ass-publickey

# To get default net device use

```shell
ip -o -4 route show to default | awk '{print $5}'
```

```shell
echo '
[Interface]
Address = 10.2.0.1/16
ListenPort = 5223
PrivateKey = YL0127lOngvvuMYalR4ntFIzdcKNRwCzpNSSKmQOU04=
PostUp = iptables -t filter -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -s 10.2.0.1/16 -o eth0 -j MASQUERADE
PostDown = iptables -t filter -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -s 10.2.0.1/16 -o eth0 -j MASQUERADE
' | sudo tee /etc/wireguard/assguard.conf > /dev/null 2>&1
```

```shell
sudo systemctl enable wg-quick@assguard
sudo systemctl start wg-quick@assguard
```


## crontask 

```

*/10 * * * * /var/www/assguard/yii acct/handshake
50 * * * * /var/www/assguard/yii acct/disconnect
0 */3 * * * /var/www/assguard/yii acct/sync

```