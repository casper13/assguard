<?php

namespace app\helpers;

/**
 * Class MsChap
 */
class MsChap
{

    /**
     * Adds the parity bit to the given DES key.
     *
     * @access private
     * @param  string  $key 7-Bytes Key without parity
     * @return string
     */
    private static function desAddParity($key)
    {
        static $oddParity = [
            1,  1,  2,  2,  4,  4,  7,  7,  8,  8, 11, 11, 13, 13, 14, 14,
            16, 16, 19, 19, 21, 21, 22, 22, 25, 25, 26, 26, 28, 28, 31, 31,
            32, 32, 35, 35, 37, 37, 38, 38, 41, 41, 42, 42, 44, 44, 47, 47,
            49, 49, 50, 50, 52, 52, 55, 55, 56, 56, 59, 59, 61, 61, 62, 62,
            64, 64, 67, 67, 69, 69, 70, 70, 73, 73, 74, 74, 76, 76, 79, 79,
            81, 81, 82, 82, 84, 84, 87, 87, 88, 88, 91, 91, 93, 93, 94, 94,
            97, 97, 98, 98,100,100,103,103,104,104,107,107,109,109,110,110,
            112,112,115,115,117,117,118,118,121,121,122,122,124,124,127,127,
            128,128,131,131,133,133,134,134,137,137,138,138,140,140,143,143,
            145,145,146,146,148,148,151,151,152,152,155,155,157,157,158,158,
            161,161,162,162,164,164,167,167,168,168,171,171,173,173,174,174,
            176,176,179,179,181,181,182,182,185,185,186,186,188,188,191,191,
            193,193,194,194,196,196,199,199,200,200,203,203,205,205,206,206,
            208,208,211,211,213,213,214,214,217,217,218,218,220,220,223,223,
            224,224,227,227,229,229,230,230,233,233,234,234,236,236,239,239,
            241,241,242,242,244,244,247,247,248,248,251,251,253,253,254,254];

        $bin = '';
        for ($i = 0; $i < strlen($key); $i++) {
            $bin .= sprintf('%08s', decbin(ord($key[$i])));
        }

        $str1 = explode('-', substr(chunk_split($bin, 7, '-'), 0, -1));
        $x = '';
        foreach($str1 as $s) {
            $x .= sprintf('%02s', dechex($oddParity[bindec($s . '0')]));
        }

        return pack('H*', $x);
    }

    /**
     * Converts ascii to unicode.
     *
     * @param string $string
     * @return false|string
     */
    private static function str2unicode($string)
    {
        $unicode = '';
        for ($i=0; $i < strlen($string); $i++) {
            $a = ord($string[$i]) << 8;
            $unicode .= sprintf("%X",$a);
        }

        return pack('H*', $unicode);
    }

    /**
     * Generates a random binary challenge
     *
     * @param int $size Size of the challenge in Bytes
     * @return string
     */
    public static function generateChallenge($size)
    {
        $challenge = '';
        for($i = 0; $i < $size; $i++) {
            $challenge .= pack('C', 1 + mt_rand() % 255);
        }

        return $challenge;
    }

    /**
     * Generates the NT-HASH from the given plaintext password.
     *
     * @param string $password
     * @return string
     */
    public static function ntPasswordHash($password)
    {
        return pack('H*', hash('md4', self::str2unicode($password)));
    }

    /**
     * Generates the response.
     *
     * @param string $challenge
     * @param string $ntHash
     * @return string
     */
    public static function challengeResponse($challenge, $ntHash)
    {
        $ntHash = str_pad($ntHash, 21, "\0");

        $key   = self::desAddParity(substr($ntHash, 0, 7));
        $response1 = openssl_encrypt($challenge, 'des-ecb', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING);
        $key   = self::desAddParity(substr($ntHash, 7, 7));
        $response2 = openssl_encrypt($challenge, 'des-ecb', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING);
        $key   = self::desAddParity(substr($ntHash, 14, 7));
        $response3 = openssl_encrypt($challenge, 'des-ecb', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING);

        return $response1 . $response2 . $response3;
    }


    /**
     * MS-CHAPv2
     */

    /**
     * Generates the challenge hash from the peer and the authenticator challenge and
     * the username. SHA1 is used for this, but only the first 8 Bytes are used.
     *
     * @param string $authChallenge
     * @param string $peerChallenge
     * @param string $username
     * @return false|string
     */
    public static function challengeHash($authChallenge, $peerChallenge, $username)
    {
        return substr(pack('H*',hash('sha1', $peerChallenge . $authChallenge . $username)), 0, 8);
    }
}