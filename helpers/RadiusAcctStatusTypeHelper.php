<?php

namespace app\helpers;

/**
 * Class RadiusAcctTypeHelper
 */
class RadiusAcctStatusTypeHelper extends AbstractTypeHelper
{
    const TYPE_RADIUS_START = 1;
    const TYPE_RADIUS_STOP = 2;
    const TYPE_RADIUS_ALIVE = 3;
    const TYPE_RADIUS_ACCOUNTING_ON = 7;
    const TYPE_RADIUS_ACCOUNTING_OFF = 8;

    /**
     * @var array
     */
    protected static $types = [
        self::TYPE_RADIUS_START => 'Start',
        self::TYPE_RADIUS_STOP => 'Stop',
        self::TYPE_RADIUS_ALIVE => 'Alive',
        self::TYPE_RADIUS_ACCOUNTING_ON => 'Accounting on',
        self::TYPE_RADIUS_ACCOUNTING_OFF => 'Accounting off',
    ];
}