<?php

namespace app\helpers;

/**
 * Class RadiusAuthTypeHelper
 */
class RadiusAuthTypeHelper extends AbstractTypeHelper
{
    const TYPE_PAP = 'pap';
    const TYPE_CHAP = 'chap';
    const TYPE_MS_CHAPv1 = 'mschapv1';
    const TYPE_MS_CHAPv2 = 'mschapv2';

    /**
     * @var array
     */
    protected static $types = [
        self::TYPE_PAP => 'PAP',
        self::TYPE_CHAP => 'CHAP',
        self::TYPE_MS_CHAPv1 => 'MSCHAPV1',
        self::TYPE_MS_CHAPv2 => 'MSCHAPV2',
    ];
}
