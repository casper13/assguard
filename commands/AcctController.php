<?php

namespace app\commands;

use app\helpers\RadiusAcctStatusTypeHelper;
use app\helpers\RadiusAcctTerminateCauseType;
use app\models\Nas;
use app\models\Peer;
use app\models\Radius;
use app\services\AddressService;
use app\services\ApiService;
use Exception;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Exception as DbException;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;

/**
 * Class AcctController
 * @package app\commands
 */
class AcctController extends Controller
{
    /**
     * Remove dead peers and accounting alive peer
     *
     * @return int
     */
    public function actionIndex(): int
    {
        return ExitCode::OK;
    }

    /**
     * Update peers the latest handshake. Run every 3..30 minutes
     *
     * @throws InvalidConfigException
     */
    public function actionHandshake()
    {
        $nas = new Nas();
        $radius = new Radius();
        $timestamp = Yii::$app->formatter->asTimestamp('now');
        /** @var Peer[] $peers */
        $peers = Peer::find()->all();
        foreach ($peers as $peer) {
            $nas->updateStats($peer);
            // Acct alive peer
            if ($radius->isEnabled()) {
                if ($peer->accounted_at + $radius->getAcctInterval() - 3 <= $timestamp) {
                    $acctAlive = $radius->acct(
                        $nas,
                        $peer,
                        $timestamp,
                        RadiusAcctStatusTypeHelper::TYPE_RADIUS_ALIVE
                    );
                    if ($acctAlive) {
                        $peer->accounted_at = $timestamp;
                        $peer->save();
                    }
                } else {
                    $peer->accounted_at = $timestamp;
                    $peer->save();
                }
            } else {
                $peer->save();
            }
        }

        return ExitCode::OK;
    }

    /**
     * Delete expired peer and config. All peers with expired the latest handshake. Run every hour
     *
     * @throws DbException
     * @throws StaleObjectException
     */
    public function actionDisconnect()
    {
        $radius = new Radius();
        $nas = new Nas();
        $api = new ApiService();
        $timestamp = Yii::$app->formatter->asTimestamp('now');
        if ($radius->getHandshakeTimeout() > 0) {
            /** @var Peer[] $peers */
            $peers = Peer::find()->all();
            foreach ($peers as $peer) {
                if ($peer->latest_handshakes_at + $radius->getHandshakeTimeout() - 3 <= $timestamp) {
                    // Disconnect
                    if ($radius->isEnabled()) {
                        $acctTerminate = $radius->acct(
                            $nas,
                            $peer,
                            $timestamp,
                            RadiusAcctStatusTypeHelper::TYPE_RADIUS_STOP,
                            RadiusAcctTerminateCauseType::TYPE_RADIUS_TERM_LOST_SERVICE
                        );
                    }
                    $apiAuth = $api->disconnect($nas, $peer, $timestamp);
                    // Remove peer config
                    $nas->removePeer($peer);
                    // Remove peer DB record
                    $peer->delete();

                }
            }
        }

        return ExitCode::OK;
    }

    /**
     * Find out lost configs and lost peers and delete them. Run every few hours
     * @return int
     * @throws DbException
     * @throws InvalidConfigException
     * @throws StaleObjectException
     */
    public function actionSync()
    {
        $this->deleteLostConfig();
        $this->deleteLostPeer();

        return ExitCode::OK;
    }

    /**
     * Find out lost configs and delete them.
     *
     * @return int
     * @throws InvalidConfigException
     */
    private function deleteLostConfig()
    {
        // All peers
        /** @var Peer[] $peers */
        $peers = Peer::find()->all();
        if (empty($peers)) {
            return ExitCode::OK;
        }
        $nas = new Nas();
        // All configs
        $configsToRemove = $nas->getConfigs();
        if (empty($configsToRemove)) {
            return ExitCode::OK;
        }
        $configs = $configsToRemove;
        foreach ($peers as $peer) {
            foreach ($configs as $key => $config) {
                if ($peer->public_key == $config['pubKey']) {
                    unset($configsToRemove[$key]);
                }
            }
        }

        // $configsToRemove - configs list that have no peer
        foreach ($configsToRemove as $configToRemove) {
            $endpoint = $configToRemove['endpoint'];
            $slice = explode(':', $endpoint);
            $ipAddress = ArrayHelper::getValue($slice, 0);
            $peer = new Peer();
            $peer->public_key = $configToRemove['pubKey'];
            $peer->framed_ip_address = $configToRemove['framedIpAddress'];
            if (!empty($ipAddress)) {
                $peer->ip_address = $ipAddress;
            }
            // Remove peer config
            $nas->removePeer($peer);
        }

        return ExitCode::OK;
    }

    /**
     * Find out lost peers and delete them
     *
     * @return int
     * @throws DbException
     * @throws InvalidConfigException
     * @throws StaleObjectException
     */
    private function deleteLostPeer()
    {
        // All peers
        /** @var Peer[] $peersToRemove */
        $peersToRemove = Peer::find()->all();
        if (empty($peersToRemove)) {
            return ExitCode::OK;
        }
        $nas = new Nas();
        // All configs
        $configs = $nas->getConfigs();
        if (empty($configs)) {
            return ExitCode::OK;
        }
        $peers = $peersToRemove;
        foreach ($configs as $config) {
            foreach ($peers as $key => $peer) {
                if ($peer->public_key == $config['pubKey']) {
                    unset($peersToRemove[$key]);
                }
            }
        }

        $radius = new Radius();
        $api = new ApiService();
        $timestamp = Yii::$app->formatter->asTimestamp('now');
        // $peersToRemove - peers list that have no config
        foreach ($peersToRemove as $peerToRemove) {
            // Disconnect
            if ($radius->isEnabled()) {
                $acctTerminate = $radius->acct(
                    $nas,
                    $peerToRemove,
                    $timestamp,
                    RadiusAcctStatusTypeHelper::TYPE_RADIUS_STOP,
                    RadiusAcctTerminateCauseType::TYPE_RADIUS_TERM_LOST_SERVICE
                );
            }
            $apiAuth = $api->disconnect($nas, $peerToRemove, $timestamp);
            // Remove peer config
            $nas->removePeer($peerToRemove);
            // Remove peer DB record
            $peerToRemove->delete();
        }

        return ExitCode::OK;
    }
}
