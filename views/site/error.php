<?php

use yii\web\Response;
use yii\web\View;

/* @var $this View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$errorCode = 404;

if (preg_match('/\#\d+/ui', $name, $match) && !empty($match[0])) {
    $errorCode = substr($match[0], -3);
}

if (!array_key_exists($errorCode, Response::$httpStatuses)) {
    $errorCode = 404;
}

$errorName = Response::$httpStatuses[$errorCode];

$this->title = $errorName;
?>
<center><h1><?=$errorCode?> <?=$errorName?></h1></center>
<hr><center>nginx</center>
