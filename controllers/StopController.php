<?php

namespace app\controllers;

use app\models\ApiRequest;
use app\models\DisconnectResponse;
use app\models\RestController;
use app\services\ConnectService;
use Yii;
use yii\base\Exception as BaseException;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class DisconnectController
 * @package app\controllers
 */
class StopController extends RestController
{
    /**
     * @inheritdoc
     */
    protected function verbs(): array
    {
        $verbs = parent::verbs();
        $verbs['index'] = ['post', 'options'];

        return $verbs;
    }

    /**
     * @return ApiRequest|DisconnectResponse
     * @throws HttpException
     * @throws BaseException
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $time = Yii::$app->formatter->asTimestamp('now');
        $ipAddress = Yii::$app->request->userIP;

        $model = new ApiRequest();
        $model->scenario = ApiRequest::SCENARIO_STOP;
        $model->load(Yii::$app->request->post());

        $service = new ConnectService();
        $disconnectResponse = $service->disconnect($model, $time, $ipAddress);
        if (!empty($disconnectResponse)) {
            return $disconnectResponse;
        }

        if ($model->hasErrors()) {
            return $model;
        }

        throw new HttpException(418, 'Disconnecting failed');
    }
}
