<?php

namespace app\controllers;

use app\models\ApiRequest;
use app\models\ConnectResponse;
use app\models\RestController;
use app\services\ConnectService;
use Yii;
use yii\base\Exception as BaseException;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class ConnectController
 * @package app\controllers
 */
class StartController extends RestController
{
    /**
     * @inheritdoc
     */
    protected function verbs(): array
    {
        $verbs = parent::verbs();
        $verbs['index'] = ['post', 'options'];

        return $verbs;
    }

    /**
     * @return ApiRequest|ConnectResponse
     * @throws HttpException
     * @throws BaseException
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $time = \Yii::$app->formatter->asTimestamp('now');
        $ipAddress = Yii::$app->request->userIP;

        $model = new ApiRequest();
        $model->scenario = ApiRequest::SCENARIO_START;
        $model->load(Yii::$app->request->post());

        $service = new ConnectService();
        $connectResponse = $service->connect($model, $time, $ipAddress);
        if (!empty($connectResponse)) {
            return $connectResponse;
        }

        if ($model->hasErrors()) {
            return $model;
        }

        throw new HttpException(418, 'Connecting failed');
    }
}
